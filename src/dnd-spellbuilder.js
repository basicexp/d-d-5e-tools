/**
 * The original version was simply too complex and was fighting with it's self to accomplish
 * the relatively simple task of creating an easy form (set of forms) to create a spell
 * 
 * Version 1.0 is designed around a simple spell object that repersents how spells are described
 * in the Dungeons and Dragons 5e's manual, and not concerned about how they actually work
 * in the game.
 * 
 * It is up the designer to consider how it will function, a seperate tool could be used
 * to help discern whether a spell is "good", this tools simply makes creating spells easy.
 * 
 * This tool takes simple data and transforms it into an HTML page that looks like what
 * you might see in the "spells" section of a D&D book.
 * 
 *  Designed and Programmed by Dustin K. C. Irvine ( BasicExp )
 *  Credit to Liam Johnson for helping tweek original concept
 *  Date: June 2nd 2018
 * 
 *  Future Features
 * 
 *      Flavor Text for components
 *      Handling special cases when it comes to generating strings
 *      Add meaningful defaults to some fields (name, etc)
 * 
 *  New to this version
 * 
 *      All Description and Optionals to recognize paragraph breaks
 *      
 *      
 *      This has been implemented, but depending on how you want to
 *      use the class, for example with Angular, it requires handling through the
 *      tool you choose to use as a controller. If you use a custom jQuery controller,
 *      there are functions that will do this conversion for you.
 * 
 */

function Spell() {

    /** 
    * Spell Related Enumerations and there corresponding
    * function groupings for accessing them outside the 
    * scope of the spell object. This is to help prevent
    * corruption of values that should never change, something
    * easily done in JavaScript
    */

    const ENUM_LEVELS = {
        0 : "Cantrip", 
        1 : "1st", 
        2 : "2nd", 
        3 : "3rd",
        4 : "4th", 
        5 : "5th", 
        6 : "6th", 
        7 : "7th",
        8 : "8th", 
        9 : "9th",
        count : 10
    }
    
    const ENUM_SCHOOLS = {
        0 : "Evocation",
        1 : "Abjuration",
        2 : "Conjuration",
        3 : "Enchantment",
        4 : "Illusion",
        5 : "Necromancy",
        6 : "Transmutation",
        count : 7
    }

    const ENUM_CAST_TIME_UNITS = {
        0 : "bonus action",
        1 : "action",
        2 : "minute",
        3 : "hour",
        4 : "day",
        5 : "week",
        6 : "month",
        7 : "year",
        count : 8
    }

    const ENUM_RANGES = {
        0 : "Self",
        1 : "Touch",
        2 : "Sight",
        3 : "Special",
        4 : "Unlimited",
        count : 5
    }

    const ENUM_COMPONENTS = {
        0 : "V",
        1 : "S",
        2 : "M",
        count : 3
    }

    const ENUM_DURATIONS = {

        units : {
            0 : "round",
            1 : "minute",
            2 : "hour",
            3 : "day",
            4 : "week",
            5 : "year",
            6 : "Instantaneous",
            7 : "Until dispelled",
            count : 8
        }

    }

    /**
     * These are a grouped set of functions to allow you interact with 
     * the enumerated spell levels without being able to corrupt the
     * values directly (without editing the source code directly)
     * 
     * These groupings are near identical across enumerations, but I could
     * not think of a good design to avoid repeating this code without
     * making things ridiculously complicated
     * 
     */
    this.LEVELS = {
        // Get a value based on a 0-based index
        get : function( value ) { return ENUM_LEVELS[value]; },
        // Get a complete list of the levels
        getAll : function() {
            
            var array = new Array();

            this.forEach( function(value) {
                array.push(value);
            });

            return array;
        },
        // Get the count
        count : ENUM_LEVELS['count'],
        // See if a value exists in the collection
        contains : function( value ) {

            var i = 0;
            
            while( i < ENUM_LEVELS.count ) {
                if ( value == ENUM_LEVELS[i++] ) {
                    return true
                }
            }

            return false;

        },
        // Call a function that takes in a single string value
        // for each value in the collection and manipulates it
        // however you see fit
        forEach : function( func ) {

            var i = 0;
            while( i < ENUM_LEVELS.count ) {
                (func(ENUM_LEVELS[i++]))
            }

        }
    }

    this.SCHOOLS = {
        // Get a value based on a 0-based index
        get : function( value ) { return ENUM_SCHOOLS[value]; },
        // Get a complete list of the schools
        getAll : function() {
            
            var array = new Array();

            this.forEach( function(value) {
                array.push(value);
            });

            return array;
        },
        // Get the count
        count : ENUM_SCHOOLS['count'],
        // See if a value exists in the collection
        contains : function( value ) {

            var i = 0;
            
            while( i < ENUM_SCHOOLS.count ) {
                if ( value == ENUM_SCHOOLS[i++] ) {
                    return true
                }
            }

            return false;

        },
        // Call a function that takes in a single string value
        // for each value in the collection and manipulates it
        // however you see fit
        forEach : function( func ) {

            var i = 0;
            while( i < ENUM_SCHOOLS.count ) {
                (func(ENUM_SCHOOLS[i++]))
            }

        }
    }

    this.CAST_TIME_UNITS = {
        // Get a value based on a 0-based index
        get : function( value ) { return ENUM_CAST_TIME_UNITS[value]; },
        // Get a complete list of the levels
        getAll : function() {
            
            var array = new Array();

            this.forEach( function(value) {
                array.push(value);
            });

            return array;
        },
        // Get the count
        count : ENUM_CAST_TIME_UNITS['count'],
        // See if a value exists in the collection
        contains : function( value ) {

            var i = 0;
            
            while( i < ENUM_CAST_TIME_UNITS.count ) {
                if ( value == ENUM_CAST_TIME_UNITS[i++] ) {
                    return true
                }
            }

            return false;

        },
        // Call a function that takes in a single string value
        // for each value in the collection and manipulates it
        // however you see fit
        forEach : function( func ) {

            var i = 0;
            while( i < ENUM_CAST_TIME_UNITS.count ) {
                (func(ENUM_CAST_TIME_UNITS[i++]))
            }

        }
    }

    this.RANGES = {
        // Get a value based on a 0-based index
        get : function( value ) { return ENUM_RANGES[value]; },
        // Get a complete list of the ranges
        getAll : function() {
            
            var array = new Array();

            this.forEach( function(value) {
                array.push(value);
            });

            return array;
        },
        // Get the count
        count : ENUM_RANGES['count'],
        // See if a value exists in the collection
        contains : function( value ) {

            var i = 0;
            
            while( i < ENUM_RANGES.count ) {
                if ( value == ENUM_RANGES[i++] ) {
                    return true
                }
            }

            return false;

        },
        // Call a function that takes in a single string value
        // for each value in the collection and manipulates it
        // however you see fit
        forEach : function( func ) {

            var i = 0;
            while( i < ENUM_RANGES.count ) {
                (func(ENUM_RANGES[i++]))
            }

        }
    }

    this.COMPONENTS = {
        get : function( value ) { return ENUM_COMPONENTS[value]},
        getAll : function() {
            var array = new Array();
            for ( var i = 0 ; i < ENUM_COMPONENTS.count; i++) {
                array.push(ENUM_COMPONENTS[i]);
            }
            return array;
        }
    }

    this.DURATIONS = {
        // Get a value based on a 0-based index
        getUnits : function( value ) { return ENUM_DURATIONS.units[value]; },
        // Get a complete list of the levels
        getAllUnits : function() {
            
            var array = new Array();

            this.forEachUnit( function(value) {
                array.push(value);
            });

            return array;
        },
        // Get the count
        unitsCount : ENUM_DURATIONS.units['count'],
        // See if a value exists in the collection
        unitsContains : function( value ) {

            var i = 0;
            
            while( i < ENUM_DURATIONS.units.count ) {
                if ( value == ENUM_DURATIONS.units[i++] ) {
                    return true
                }
            }

            return false;

        },
        // Call a function that takes in a single string value
        // for each value in the collection and manipulates it
        // however you see fit
        forEachUnit : function( func ) {

            var i = 0;
            while( i < ENUM_DURATIONS.units.count ) {
                (func(ENUM_DURATIONS.units[i++]))
            }

        }
    }

    // Private member variables of the spell that cannot be
    // accessed outside the spell

    var name;
    var level;
    var school;
    var isRitual;
    var castTimeValue;
    var castTimeUnits;
    var range;
    var components;
    var duration;
    var isConcentration;
    var description;
    var optionals = new Array();

    // Function as 'Properties' in C#, if you hand them nothing
    // they will return the value, otherwise they will attempt
    // to set the internal property barring internal validation
   
    /**
     * Getter/Setter for a spell's name
     *  Examples: 
     *      spellVar.Name(); returns current value of the spell name
     *      spellVar('New Spell'); will attempt to set the spell name to
     *          'New Spell'
     * 
     * @param { string } value must be a string
     */
    this.Name = function( value ) {  

        if ( value == null ) {
            return name;
        } else if ( typeof(value) == "string" ) {
            name = value;
        } else {
            throw "Invalid Value: spell.Name must be a string";
        }

    }

    /**
     * Getter/Setter for a spell's level
     * 
     *  Examples:
     *      spellVar.Level(); returns the current value of the spell level
     *      spellVar.Level("Cantrip") will attempt to set the spell level
     *          to "Cantrip".
     * 
     * @param { string } value A spell level must be contained within the 
     * enumeration LEVELS.getAll()
     */
    this.Level = function( value ) {  
        if ( value == null ) {
            return level; 
        } else if ( this.LEVELS.contains(value) ) {
            level = value;
        } else {
            throw "Invalid Value: spell.Level set value must " + 
                    "be contained within LEVELS.getAll()";
        }
    }

    /**
     * Getter/Setter for a spell's school
     * 
     *  Examples:
     *      spellVar.School(); returns the current value of the spell school
     *      spellVar.School("Evocation") will attempt to set the spell level
     *          to "Evocation".
     * 
     * @param { string } value A spell school must be contained within 
     * the enumeration SCHOOLS.getAll()
     */
    this.School = function( value ) { 
        if ( value == null ) {
            return school; 
        } else if ( this.SCHOOLS.contains(value) ) {
            school = value;
        } else {
            throw "Invalid Value: spell.School set value must " + 
                    "be contained within SCHOOLS.getAll()";
        }
     }

    /**
     * Getter/Setter for whether a spell is a ritual or not using a 
     * boolean 
     * 
     *     Example:
     *         spellVar.IsRitual(); returns whether the spell is a ritual
     *         spellVar.IsRitual(true); will attempt to set spell to be
     *             a ritual
     * 
     * @param { boolean } value ( 0 and 1 will not work as booleans )
     */
    this.IsRitual = function( value ) { 
        if ( value == null ) {
            return isRitual;
        } else if ( typeof(value) == "boolean" ) {
            isRitual = value;
        } else {
            throw "Invalid Value: spell.IsRitual must be a boolean";
        }
    }

    /**
     * Getter/Setter for a spell's cast time, both value and units. 
     *  Unit plurals are handled internally
     *  
     *  Examples:
     *      spellVar.CastTime() returns the value and units as a single string
     *      spellVar.CastTime( 1, "action" ) will attempt to set a spell cast
     *          time and to "1 action"
     *      spellVar.CastTime( 3, "minute" ) will attempt to set a spell cast
     *          time and to "3 minutes"
     * 
     * @param { number } value 
     * @param { string } units 
     */
    this.CastTime = function( value, units ) { 
        if ( value == null && units == null ) {
            return castTimeValue + " " + castTimeUnits;
        }

        if ( value < 1 || typeof(value) != "number" || isNaN(value) ) {
            throw "Invalid Value: spell.CastTime values must be a number and "
            + "greater than or equal to 1";
        }

        if ( !this.CAST_TIME_UNITS.contains(units) ) {
            throw "Invalid Value: spell.CastTime units must be contained "
            + "within CAST_TIME_UNITS.getAll()";
        }

        castTimeValue = value;
        castTimeUnits = value > 1 ? units + "s" : units; 

     }

    /**
     * Getter/Setter for a spell's range. 
     * 
     *  Example:
     *      spellVar.Range() returns the current range of the spell
     *      spellVar.Range(10) will attempt to set the spell range to "10 feet"
     *      spellVar.Range("Self") will attempt to set the spell range to "Self"
     * 
     * @param { any } value Ranges must either be a number that is evenly
     * divisble by 5 or be contained within the RANGES.getAll()
     */
    this.Range = function( value ) { 
        if ( value == null ) {
            return range;
        } else if ( typeof(value) == "number" && value % 5 == 0  ) {
            range = value + " ft.";
        } else if ( this.RANGES.contains(value) ) {
            range = value;
        } else {
            throw "Invalid Value: spell.Range must either be a number evenly "
                + "divisible by 5 or be contained within RANGES.getAll()";
        }
    }

    /**
     * 
     * Getter/Setter for the components of a spell 
     * 
     *  Example:
     *      spellVar.Components() returns a string repersenting the current
     *          components
     *      spellVar.Componenets( true, true, true ) will set the components
     *          to "V, S, M"
     * 
     * @param { boolean } verbal ( 0 and 1 will not work as booleans )
     * @param { boolean } somatic ( 0 and 1 will not work as booleans )
     * @param { boolean } material ( 0 and 1 will not work as booleans )
     */
    this.Components = function( verbal, somatic, material ) {

        if ( verbal == null && somatic == null && material == null ) {
            return components;
        } else if ( verbal != null && somatic != null && material != null && 
            typeof(verbal) == "boolean" && typeof(somatic) == "boolean" && 
            typeof(material) == "boolean") {

                components = "";
                components += verbal ? ENUM_COMPONENTS[0] + ", " : "";
                components += somatic ? ENUM_COMPONENTS[1] + ", " : "";
                components += material ? ENUM_COMPONENTS[2] + ", " : "";
                components = components.substring(0, components.length - 2 );
                components = components == "" ? "None" : components;

        } else {
            throw "Invalid Value: spell.Component only takes in booleans and all three "
             + "fields must be defined";
        }

    }
    
    /**
     * Getter/Setter for a spell's duration
     * 
     *  Examples:
     *      spellVar.Duration() returns the current durations as a string
     *      spellVar.Duration( 1, "minute" ) will attempt to set the spell duration
     *          to "1 minute"
     *      spellVar.Duration( "Until dispelled" ) will attempt to set the spell
     *          duration to "Unti dispelled"
     *      spellVar.Duration( "2", "round" ) will attempt to set the spell duration
     *          to "2 minutes" 
     * 
     * @param {string / number} value 
     * @param { string (optional) } units 
     */
    this.Duration = function( value, units ) { 
        if ( value == null && units == null ) {
            return duration;
        } else if ( typeof(value) == "number" &&  value > 0 && isNaN(value) == false
                    && this.DURATIONS.unitsContains( units ) && 
                    (this.DURATIONS.getUnits(6) != units || 
                    this.DURATIONS.getUnits(7) != units) ) {
            duration = value > 1 ? value + " " + units + "s" :
                        value + " " + units;
        } else if ( this.DURATIONS.getUnits(6) == units || 
                    this.DURATIONS.getUnits(7) == units ) {
            duration = units;
        } else {
            throw "Invalid Value: spell.Duration must either be a number greater than "
                + "or equal to one with units that are contained within "
                + "DURATION.getAllUnits() or spell.Duration must be a value contained "
                + "in DURAION.getAllValues";
        }
    }

    /**
     * Getter/Setter
     * 
     *  Examples:
     *      spellVar.IsConcentration() returns whether the spell is a concentration
     *          or not
     *      spellVar.IsConcentration(true) will attempt to set the concentration of
     *          this spell to true
     * 
     * @param {boolean} value 
     */
    this.IsConcentration = function( value ) { 
        if ( value == null ) {
            return isConcentration;
        } else if ( typeof(value) == "boolean" ) {
            isConcentration = value;
        } else {
            throw "Invalid Value: spell.IsConcentration must be set to a boolean value";
        }
    }

    /**
     * Getter/Setter for a spell's description
     *  Examples: 
     *      spellVar.Description(); returns current value of the spell description
     *      spellVar('New description'); will attempt to set the spell description 
     *      to 'New description'
     * 
     * @param { string } value must be a string
     */
    this.Description = function( value ) {
        if ( value == null ) {
            return description;
        } else if ( typeof(value) == "string" ) {
            description = value
        } else {
            throw "Invalid Value: spell.Description must be a string";
        }
    }

    /**
     * GetOptionals returns a deep copy of all of the optionals in 
     * an array format
     */
    this.GetOptionals = function() { 
        var safeArray = new Array();
        optionals.forEach( function( item, index ) {
            if ( item != "undefiend" ) {
                safeArray.push( item );
            }
        });
        return safeArray;
    }

    /**
     * Add/Edit an optional, the addition will fail if all fields
     * are not defined. This function overwrites optionals when 
     * you specify an index that is already taken.
     * 
     * @param { string } title 
     * @param { string } description 
     */
    this.AddOptional = function( index, title, description ) {
        if ( typeof(index) == "number" && typeof(title) == "string" && typeof(description) == "string" ) {
            optionals[index] = { 'title' : title, 'description' : description }
            
        }
    }

    /**
     * Remove an optional based on its index
     * 
     * @param { number } index 
     */
    this.RemoveOptional = function( index ) {
        if ( optionals[index] != "undefined" ) {
            optionals[index] = "undefined";
        }
    }

    /*
        This spell block is for the generating of strings from 
        interal spell properties
    */

    /**
     * Public helper for returning any string with every word
     * capitalized title
     * 
     * @param { str }
     */ 
    this.Titlize = function( str ) {
                // Capitalize the first Letter
                var titleParts = str.split(" ");
                var title = "";
        
                for ( i = 0 ; i < titleParts.length ; i++) {
                    title += titleParts[i].substring(0,1).toUpperCase() +
                        titleParts[i].substring(1) + " ";
                }
        
                return title.trim();
    }

    /**
     * Returns a formatted string, including appropriate capitalization
     * for the Spell title; the spell name with all caps on first letters.
     */
    this.getTitleString = function() { 

        return this.Titlize(this.Name());

    }

    /**
     * Returns a formatted string, including the spell level, school and
     * whether it can be cast as a ritual. Intended to be printed bellow 
     * the spell Title
     */
    this.getSubTitleString = function() {
        var subtitle = 
            this.Level() == "Cantrip" ? 
            this.Level() : 
            this.Level() + "-level"; // Add Level
        subtitle += " " + this.School(); // Add School
        subtitle += this.IsRitual() ? " (ritual)" : ""; // Add (if) Ritual
        return subtitle;
    }

    /**
     * Currently just returns this.CastTime() but exists for consistency's
     * sake
     */
    this.getCastTimeString = function() {
        return this.CastTime();
    }

    /**
     * Currently just returns this.Range() but exists for consitency's
     * sake
     */
    this.getRangeString = function() {
        return this.Range();
    }

    /**
     * Currently just return this.Components() but exists for consitency's
     * sake
     */
    this.getComponentsString = function() {
        return this.Components();
    }

    /**
     * Returns the duration with the spell formated to include whether the
     * spell is concentration or not.
     */
    this.getDurationString = function() {
        return this.IsConcentration() ?
            "Concentration up to " + this.Duration() :
            this.Duration();
    }

    /**
     * Returns the description after converting newline characters
     */
    this.getDescriptionString = function() {
        return this.convertNewLineChars(this.Description());
    }

    /**
     * Returns an array of the Optional Titles where the indecies of this array
     * matches to the indecies of the getOptionalDescriptionStrings() array 
     */

    this.getOptionalsTilteStrings = function() {
        
        var titles = new Array();

        // JS is weird, context issue causes me to need to do this
        var tempSpell = new Spell();

        optionals.forEach( function( val, i ) {
            if (val != "undefined") {
                titles.push( tempSpell.Titlize(val.title) );
            }
        });
    
        return titles; 

    }

    /**
     * Returns an array of the Optional Titles where the indecies of this array
     * matches to the indecies of the getOptionalTitleStrings() array
     */

    this.getOptionalsDescriptionStrings = function() {
        
        var desc = new Array();
        var tempSpell = new Spell();

        optionals.forEach( function( val, i ) {
            if ( val != "undefined") {
                desc.push( tempSpell.convertNewLineChars(val.description));
            }
        });
    
        return desc; 

    }

    /**
     * 
     * Returns the same string with the new line characters replace with
     * HTML < br/> tags
     * 
     * @param { string } str 
     */
    this.convertNewLineChars = function( str ) {

        while ( str.indexOf('\n') != -1 ) {
            str = str.substring(0, str.indexOf('\n')) + '<br\\>' + str.substring(str.indexOf('\n') + 1)
        }

        return str;
    }

}