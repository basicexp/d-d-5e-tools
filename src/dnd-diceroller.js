/**
 * The following JavaScript code is written to define an object and 
 * its behaviour, such that it will make it easy to replicate
 * the rolling of dice in a game of 5e Dungeons and Dragons
 * 
 * Dustin "BasicExp" Irvine
 * December 28 2018
 * 
 */

 function DiceRoller() {
    
    //                          //
    // Private Member Variables //
    //                          //

    // Sum mode is used to indicate if consecutive rolls,
    // possibly of multiples of dice are to be added together 
    // or treated as seperate rolls 
    var sumMode = false;
    // Number of times to roll the dice
    var rollCount = 1;
    // Total of rolls
    var rollTotal = 0;

    //                  //
    // Public Functions //
    //                  //

    /** 
     * C# Style Getter / Setter. Returns the current state if nothing is
     * passed in or sets the state if handed a boolean value
     * 
     * @param { boolean } value
    */
    this.SumMode = function( value ) {
        if ( value == null ) {
            return sumMode;
        } else if ( typeof(value) == "boolean" ) {
            sumMode = value;
        } else {
            throw "Invalid Value: SumMode can only take in a single value and it must be boolean."
        }
    }

    /**
     * 
     * C# Getter / Setter. Returns the current RollCount if nothing is
     * passed in or sets the roll count if a number greater than or
     * equal to 1 is passed in.
     * 
     * Defaults to 1, if not set previous to rolling
     * 
     * @param { number } value 
     */
    this.RollCount = function( value ) {
        if ( value == null ) {
            return rollCount;
        } else if ( typeof(value) == "number" && value >= 1) {
            rollCount = value;
        } else {
            throw "Invalid Value: RollCount must be a number, 1 or larger."
        }
    }   

    /**
     * Getter only, returning the current roll total. Roll total
     * cannot be directly accessed.
     */
    this.RollTotal = function() {
        return rollTotal;
    }

    /**
     * Takes in a number that repersents the number of sides on a
     * dice you want to pretend to roll. It will roll that dice
     * a number of times equal to the dice roll count, adding the 
     * results
     * 
     * Note: Sum Mode is for adding consecutive rolls of some number of dice and
     * Roll Count decides how many times to roll a single dice for one call 
     * of RollAD
     * 
     * @param { number } numberOfSides
     */
    this.RollAD = function( numberOfSides ) {

        if ( // Validate passed in number
            numberOfSides == null || 
            typeof(numberOfSides) != "number"||
            numberOfSides < 1
        ) {
            throw "Invalid Value: Number of sides must be a number, 1 or greater";
        }

        // If we are not in sum mode, reset rollTotal
        if ( !this.SumMode()) {
            rollTotal = 0;
        }

        // Loop through a number of times equal to roll count
        // and add the rolls
        for ( var i = 0 ; i < this.RollCount() ; i++ ) {
            rollTotal += Math.floor((Math.random() * numberOfSides) + 1);
        }

        // Return the roll results
        return this.RollTotal();

    }

    /**
     * Clears the current dice roll total and sets it back to zero
     */

    this.ClearDiceTotal = function() {
        rollTotal = 0;
    }

 }