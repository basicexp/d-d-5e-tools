/**
 * Dustin "BasicExp" Irvine
 * Decemeber 26th 2018
 * 
 * This is a simple Angular Module used bind a Spell object
 * to html elements such that they auto update when the
 * form is updated. 
 * 
 * This module requires jQuery
 */

var app = angular.module('DnD-App', []);
app.controller('SpellBuilderCtrl', function($scope) {
    
    // Spell Object
    $scope.spell = new Spell();
    
    // Initial Spell Values
    $scope.spell.initialRangeVal = 30;
    $scope.spell.initialDurationVal = 1; 
    $scope.spell.initialCastTimeVal = 1;
    $scope.spell.initialRitualVal = false;
    $scope.spell.initialComponentsVal = true;

    // Set initial spell values
    $scope.spell.Name("Spell Name Here");
    $scope.spell.Level($scope.spell.LEVELS.get(0));
    $scope.spell.School($scope.spell.SCHOOLS.get(0));
    $scope.spell.IsRitual($scope.spell.initialRitualVal);
    $scope.spell.CastTime(
        $scope.spell.initialCastTimeVal, 
        $scope.spell.CAST_TIME_UNITS.get(0)
        );
    $scope.spell.Range($scope.initialRangeVal);
    $scope.spell.Components( 
        $scope.spell.initialComponentsVal,
        $scope.spell.initialComponentsVal, 
        $scope.spell.initialComponentsVal
    );
    $scope.spell.Duration( 
        $scope.spell.initialDurationVal, 
        $scope.spell.DURATIONS.getUnits(0)
    );
    $scope.spell.IsConcentration(false);
    $scope.spell.Description("Spell Description Here");

    // Event Handlers

    // Spell Title
    $scope.updateTitle = function() {
        try {
            $scope.spell.Name( jqSpellName.val().trim() );
            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    };

    // Spell Subtitle
    $scope.updateSubTitle = function() {
        try {
            $scope.spell.Level( jqSpellLevel.val() );
            $scope.spell.School( jqSpellSchool.val() );
            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    };

    // Ritual Button 
    $scope.ritualToggle = function() {
            // Fetch the current status of the ritual button
            var isRitual = jqSpellRitual.attr('dkci-value') == "true";
            // Toggle values based on result
            try {
                if ( isRitual ) {
                    jqSpellRitual.attr("dkci-value", "false");
                    jqSpellRitual.addClass("false");
                    jqSpellRitual.removeClass("true");
                    $scope.spell.IsRitual(false);
                } else {
                    jqSpellRitual.attr("dkci-value", "true");
                    jqSpellRitual.addClass("true");
                    jqSpellRitual.removeClass("false");
                    $scope.spell.IsRitual(true);
                }
                $scope.updateCastingTime();
                ClearSpellError();
            } catch (e) {
                ClearSpellError();
                PrintSpellError(e);
            }
    }

    // Spell Casting Time
    $scope.updateCastingTime = function() {
        try {
            $scope.spell.CastTime(
                parseInt(jqSpellCastTimeValue.val()), 
                jqSpellCastTimeUnits.val()
            );
            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    }

    $scope.updateRange = function($event) {
        try {
            // Fetch a jq reference to what was clicked
            var jqClicked = $($event.currentTarget);

            // Iterate through and adjust values
            jqSpellRangeUnits.each( function ( index, element ) {
                element = $(element);
                if ( element.text() != jqClicked.text() ) {
                        element.attr("dkci-value", "false")
                        element.removeClass("true");
                        element.addClass("false");
                } else {
                    jqClicked.attr("dkci-value", "true");
                    element.removeClass("false");
                    element.addClass("true");
                }
            });

            // Adjust Spell
            
            // If we are assigning a value based on a number, 
            // hand it the number
            if ( jqClicked.text().trim() == "ft." ) {
                $scope.spell.Range(parseInt(jqSpellRangeValue.val()));
            // Otherwise just hand it the units
            } else {
                $scope.spell.Range(jqClicked.text().trim());
            }

            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    }

    $scope.updateComponents = function($event) {
        // Fetch a jq reference to what was clicked
        jqClicked = $($event.currentTarget);
        // Toggle values for current item
        if ( jqClicked.attr("dkci-value") == "true") {
            jqClicked.attr("dkci-value", "false");
            jqClicked.addClass("false");
            jqClicked.removeClass("true");
        } else {
            jqClicked.attr("dkci-value", "true");
            jqClicked.addClass("true");
            jqClicked.removeClass("false");
        }
    
        // Temp references for components
        var v = jqSpellV.attr("dkci-value") == "true";
        var s = jqSpellS.attr("dkci-value") == "true";
        var m = jqSpellM.attr("dkci-value") == "true";

        // Adjust Spell
        try {
            $scope.spell.Components(v, s, m);
            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }

    }

    $scope.toggleConcentration = function() {
        // Fetch the current status of the concentration button
        var isConcentration = jqSpellConcentration.attr("dkci-value") == "true";

        // Adjust based on the status, functioning as a toggle
        try {
            if ( isConcentration ) {
                jqSpellConcentration.attr("dkci-value", "false");
                jqSpellConcentration.addClass("false");
                jqSpellConcentration.removeClass("true");
                $scope.spell.IsConcentration(false);
            } else {
                jqSpellConcentration.attr("dkci-value", "true");
                jqSpellConcentration.addClass("true");
                jqSpellConcentration.removeClass("false");
                $scope.spell.IsConcentration(true);
            }
            $scope.updateDuration();
            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    }

    $scope.updateDuration = function() {
        // Fetch the current units selected
        var units = jqSpellDurationUnits.val().trim()

        try {
            // If the units do not require a value, ignore and disable it
            if ( units == "Instantaneous" || units == "Until dispelled") {
                jqSpellDurationValue.prop("disabled", true);
                $scope.spell.Duration(null, units);
            // If the units require a value, enable and use it
            } else {
                var value = parseInt(jqSpellDurationValue.val());
                jqSpellDurationValue.prop("disabled", false);
                $scope.spell.Duration(value, units);
            }
            ClearSpellError();
        } catch(e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    }

    $scope.updateDescription = function($event) {
        try {
            $scope.spell.Description(jqSpellDescription.val().trim());
            ClearSpellError();
        } catch (e) {
            ClearSpellError();
            PrintSpellError(e);
        }
    };

});

// This filter is what lets us filter new line characters into
// <br> characters on the page without any additional security checks
// being necessary

// Credit To: Hardik Savani
// Link: http://itsolutionstuff.com/post/angularjs-nl2brtextarea-newline-and-linebreak-conversion-example-with-demoexample.html

app.filter('nl2br', function($sce){
    return function(msg,is_xhtml) { 
        var is_xhtml = is_xhtml || true;
        var breakTag = (is_xhtml) ? '<br />' : '<br>';
        var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
        return $sce.trustAsHtml(msg);
    }
});

/**
 * This Contorller is for the handling of the Dice Roller Tool
 */
app.controller('DiceRollerCtrl', function($scope) {

    // Dice Roller object
    $scope.diceRoller = new DiceRoller();

    // An Array to represent the dice we want to roll
    $scope.dice = [ 4, 6, 8, 10, 12, 20, 100 ];

    // Increment Roll Count by one
    $scope.RollCountPlusOne = function() {
        $scope.diceRoller.RollCount($scope.diceRoller.RollCount() + 1);
    }

    // Decriment Roll Count by one
    $scope.RollCountMinusOne = function() {
        
        if ( $scope.diceRoller.RollCount() > 0 ) {
            $scope.diceRoller.RollCount($scope.diceRoller.RollCount() - 1);
        }

    }

    // Toggle Sume Mode
    $scope.ToggleSumMode = function() {
        $scope.diceRoller.SumMode( !$scope.diceRoller.SumMode() );
        if ( $scope.diceRoller.SumMode()) {
            jqDiceSumMode.addClass("sum-mode-on");
        } else {
            jqDiceSumMode.removeClass("sum-mode-on");
        }
    }

    // Roll the dice
    $scope.RollDice = function( $event ) {
        // Fetch reference to what was clicked
        var jqDie = $($event.currentTarget);
        // Fetch the value of what was clicked
        var numberOfSides = parseInt(jqDie.attr("dkci-value"));
        // Roll dice
        $scope.diceRoller.RollAD(numberOfSides);
    }

});