/**
 * Dustin "BasicExp" Irvine
 * Decemeber 26th 2018
 * 
 * This script is specific to the loading of jq objects and
 * their manipulation. This script should be loaded before
 * any other page specific scripts so all scripts can make use
 * of any jq objects loaded.
 * 
 */

 var jqSpellBuilderBtn;
 var jqDiceRollerBtn;
 var jqSpellContainer;
 var jqDiceRollerContainer;
 var jqSpellName;
 var jqSpellLevel;
 var jqSpellSchool;
 var jqSpellRitual
 var jqSpellCastTimeValue;
 var jqSpellCastTimeUnits;
 var jqSpellRangeValue;
 var jqSpellRangeUnits;
 var jqSpellV;
 var jqSpellS;
 var jqSpellM;
 var jqSpellDurationValue;
 var jqSpellDurationUnits;
 var jqSpellConcentration;
 var jqSpellDescription;
 var jqAngularSpellDescription;
 var jqSpellError;
 var jqDiceSumMode;

 // On Load
 $(function() {

    // Generate the jq objects we need
    jqSpellBuilderBtn = $(".spell-builder");
    jqDiceRollerBtn = $(".dice-roller");
    jqSpellContainer = $(".ang-spell-container");
    jqDiceRollerContainer = $('.dice-roller-container');
    jqSpellName = $(".spell-name");
    jqSpellLevel = $(".spell-level");
    jqSpellSchool = $(".spell-school");
    jqSpellRitual = $(".spell-ritual");
    jqSpellCastTimeValue = $(".spell-cast-value");
    jqSpellCastTimeUnits = $(".spell-cast-units");
    jqSpellRangeValue = $(".spell-range-number");
    jqSpellRangeUnits = $(".spell-range-option");
    jqSpellV = $("#V");
    jqSpellS = $("#S");
    jqSpellM = $("#M");
    jqSpellDurationValue = $(".spell-duration-value");
    jqSpellDurationUnits = $(".spell-duration-units");
    jqSpellConcentration = $(".spell-concentration");
    jqSpellDescription = $(".spell-description");
    jqAngularSpellDescription = $('.ang-spell-description');
    jqSpellError = $(".spell-error");
    jqDiceSumMode = $('.ang-sum-mode');

    // Event Handlers for General Page Behaviour
    // All Spell Builder Event Handlers are within the Angular App

    jqSpellBuilderBtn.click( function() {
        jqSpellContainer.css("display", "block");
        jqDiceRollerContainer.css("display", "none");
        jqSpellBuilderBtn.addClass("true");
        jqSpellBuilderBtn.removeClass("false");
        jqDiceRollerBtn.addClass("false");
        jqDiceRollerBtn.removeClass("true");
    });

    jqDiceRollerBtn.click( function() {
        jqSpellContainer.css("display", "none");
        jqDiceRollerContainer.css("display", "block");
        jqSpellBuilderBtn.addClass("false");
        jqSpellBuilderBtn.removeClass("true");
        jqDiceRollerBtn.addClass("true");
        jqDiceRollerBtn.removeClass("false");
    });
    
    // Create inital state
    jqSpellBuilderBtn.click();

 });

 // Printing Errors to the Error Block
 function PrintSpellError( e ) {
     jqSpellError.append("<div class='error'>" + e + "</div>");
 }

 function ClearSpellError() {
    jqSpellError.html("");
 }
